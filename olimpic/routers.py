from rest_framework.routers import DefaultRouter,SimpleRouter
from users.views import UsuariosViewSet
from athlete.views import *

router = DefaultRouter()
router.register(r'users', UsuariosViewSet)
router.register(r'teams', TeamViewSet,)
router.register(r'sports', SportViewSet)
router.register(r'games', GamesViewSet)
router.register(r'events', EventViewSet)
router.register(r'game_event', GameEventsViewSet)
router.register(r'athletes', AthleteViewSet)
router.register(r'athletes_event', AthleteEventsViewSet)

