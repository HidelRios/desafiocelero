from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from users.models import Usuario


admin.site.register(Usuario,UserAdmin)

# Register your models here.
