from django.shortcuts import render
from users.models import Usuario
from users.serializers import UsuarioSerializer
from rest_framework import viewsets,generics

# Create your views here.

class UsuariosViewSet(viewsets.ModelViewSet):

  queryset = Usuario.objects.all()
  serializer_class = UsuarioSerializer

