from rest_framework import fields, serializers
from users.models import *

class UsuarioSerializer(serializers.ModelSerializer):

    password = serializers.CharField(
        style={'input_type': 'password'},
        write_only=True,
        label="Senha",
        max_length=100,
    )

    password_confirm = serializers.CharField(
        style={'input_type': 'password'},
        write_only=True,
        label="Confirme a senha",
        max_length=100,
        
    )

    is_staff = serializers.BooleanField(
        label="Membro da Equipe",
        help_text="Usuário consegue acessar o site de administração."
    )

    is_superuser = serializers.BooleanField(
        label="SuperUsuário",
        help_text="Concede todas as permissões ao usuário."
    )

    class Meta:
        model = Usuario
        fields = ['username','email', 'password', 'password_confirm', 'is_staff', 'is_superuser']
        extra_kwargs = {'password': {'write_only': True}}

    def save(self):
        conta = Usuario(
          username=self.validated_data['username'],
          email=self.validated_data['email'], 
          is_staff=self.validated_data['is_staff'],
          is_superuser=self.validated_data['is_superuser']
        )
        password = self.validated_data['password']
        password_confirm = self.validated_data['password_confirm']

        if password != password_confirm:
            raise serializers.ValidationError({'password': 'As senhas não são iguais.'})
        conta.set_password(password)
        conta.save()
        return conta