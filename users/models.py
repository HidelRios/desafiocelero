from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class Usuario(AbstractUser):

  username = models.CharField(max_length=100, unique=True)
  password = models.CharField(max_length=100)
  email = models.EmailField(max_length=254, unique=True, error_messages={'unique': "O email cadastrado já existe."})
  is_staff = models.BooleanField(default=False)
  is_superuser = models.BooleanField(default=False)
  
  
  def __str__(self):
      return self.username


