from django.conf.urls import url
import pytest
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from users.models import Usuario

# Create your tests here.


class UsersTests(APITestCase):

    
  def test_create_users(self):
    url = reverse('usuario-list')

    data = {
      'username': 'admin',
      'email': 'admin@gmail.com', 
      'password': 'test123',
      'password_confirm': 'test123',
      'is_staff': False,
      'is_superuser': False
      
    }
    response = self.client.post(url, data=data, format='json')
    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    self.assertEqual(Usuario.objects.count(), 1)
    self.assertEqual(Usuario.objects.get().username, 'admin')
    
  def test_get_users(self):
    url = reverse('usuario-list')
    response = self.client.get(url)
    assert response.status_code == 200



@pytest.fixture
def new_user():
  return Usuario.objects.create_user('userteste', 'testepassword', 'user@example.com')


@pytest.mark.django_db
def test_user_create(new_user):
  assert Usuario.objects.count() == 1

@pytest.mark.django_db
def test_user_create_superuser(new_user):
  assert Usuario.objects.count() == 1

@pytest.mark.django_db
def test_user_list():
  Usuario.objects.all()
  assert Usuario.objects.count() >= 0


@pytest.mark.django_db
def test_user_create_without_username():
  with pytest.raises(ValueError):
    Usuario.objects.create_user(None, 'testepassword', 'user@example.com')




