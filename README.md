# Desafio Celero - Desenvolvedor Backend Python

Repositório destinado a resolução do desafio proposto pela empresa Celero para o cargo de desenvolvedor backend Python. 
O desafio consiste na criação de uma API Rest para servir os dados do dataset [“120 years of Olympic history: athletes and results”](https://www.kaggle.com/heesoo37/120-years-of-olympic-history-athletes-and-results#athlete_events.csv)
presente no Kaggle.

## Estrutura do Projeto ##

* olimpic: Módulo pradão do Django onde contém as configurações do projeto.
* athlete: Este módulo contém os arquivos necessários para executar a aplicação Athlete. Realiza a gerência dos models da Api. 
* users:   Este módulo contém os arquivos necessários para executar a aplicação Users. Realiza a gerência da autenticação da Api.
* utils:   Este módulo contém o diagrama de classe e arquivo csv utilizado como base de dados.


## Começando ##

Realize o download do projeto na sua máquina.

### Pré-requisitos ###

* Python 3
* Django
* Django Rest Framework

### Instalação ###

Após realizar o download do projeto na sua máquina local, realize as seguintes configurações:

1.Crie um ambiente virtual

2.Dentro do ambiente virtual execute o comando abaixo:
	```
	pip3 install -r requirements.txt
	``` 
3.Navegue até a pasta olimpic onde contém o arquivo manage.py e execute o comando abaixo:
	```
	python3 manage.py migrate
	```
4.Inicie o servidor com o comando abaixo:
	```
	python3 manage.py runserver
	```

## Povoamento do Banco de Dados ##

Após os passos acima podemos realizar a migração dos dados do arquivo csv para o banco de dados do projeto.

1.Pare o servidor se ainda estiver rodando.

2.Execute o comando seguinte para povoar o banco de dados:
	```
	python3 manage.py import_csv utils/file_csv/athlete_events.csv
	```
## Execução de Testes ##

Foi desenvolvido poucos casos de testes e estes são referentes a criação de um usuário para realização do login, segue abaixo o comando para executar os testes.
	```
	pytest
	```
## Rotas ##

Para acessar as rotas de Crud das classe é necessário está logado na api, dessa forma deve-se primeiro criar um usuário.

* **/users/ -** Para criar usuário que será utilizado no login da api.
* **/teams/ -** Crud geral da Classe Team.
* **/sports/-** Crud geral da Classe Sports.
* **/games/-** Crud geral da Classe Games.
* **/events/-** Crud geral da Classe Events.
* **/game_event/-** Crud geral da Classe Game Event.
* **/athletes/-** Crud geral da Classe Athletes.
* **/athletes_event/-** Crud geral da Classe Athletes Event.
	

## Modelagem dos dados
### Diagrama de Classes ###
![Diagrama de Classe](/utils/documents/diagram_class/diagrama_class.png)

## Api online
A api encontra-se desponível no Heroku através do link [ApiDesafioCelero](https://rocky-depths-21984.herokuapp.com/).