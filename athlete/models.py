from typing import Match
from django.db import models

# Create your models here.


class Team(models.Model):
  name_team = models.CharField(max_length=200)
  noc = models.CharField(max_length=10)

  def __str__(self):
      return self.name_team


class Sport(models.Model):
  name_sport = models.CharField(max_length=200)

  def __str__(self):
      return self.name_sport

class Event(models.Model):
  name_event = models.CharField('Name',max_length=200)
  sport = models.ForeignKey(Sport,on_delete=models.CASCADE)

  def __str__(self):
      return self.name_event


class Game(models.Model):
  name_game = models.CharField('Name',max_length=200)
  season = models.CharField(max_length=200)
  year = models.IntegerField()
  city = models.CharField(max_length=200)
  events = models.ManyToManyField(Event, through='GameEvents')

  def __str__(self):
      return self.name_game

class GameEvents(models.Model):
  game = models.ForeignKey(Game, on_delete=models.CASCADE)
  event = models.ForeignKey(Event, on_delete=models.CASCADE)

  def __str__(self):
      return self.event.name_event + " - " + self.game.name_game

class Athlete(models.Model):
  SEX = (
    ('M', 'Male'),
    ('F', 'Female'),
  )
  name_athlete = models.CharField('Name',max_length=200)
  sex = models.CharField(max_length=1,choices=SEX)
  height = models.FloatField()
  weight = models.FloatField()
  team = models.ForeignKey(Team,on_delete=models.CASCADE)
  events = models.ManyToManyField(GameEvents, through='AthleteEvents')

  def __str__(self):
      return self.name_athlete


class AthleteEvents(models.Model):
  MEDAL = (
    ('GOLD', 'Gold'),
    ('SILVER', 'Silver'),
    ('BRONZE', 'Bronze'),
    ('NA', 'NA')
  )
  athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
  athlete_age = models.IntegerField(default=0)
  game_event = models.ForeignKey(GameEvents, on_delete=models.CASCADE)
  medal = models.CharField('Medal',max_length=6,choices=MEDAL)

  def __str__(self):
      return self.athlete.name_athlete + " - " + self.event.name_event + " - " +  self.medal
