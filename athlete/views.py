from django.shortcuts import render
from rest_framework.serializers import Serializer
from athlete.models import *
from athlete.serializers import *
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters


# Create your views here.

class TeamViewSet(viewsets.ModelViewSet):
  
  queryset = Team.objects.all()
  serializer_class = TeamSerializer
  permission_classes = [IsAuthenticated]
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name_team', 'noc']
  ordering_fields = ['name_team', 'noc']

class SportViewSet(viewsets.ModelViewSet):
  
  queryset = Sport.objects.all()
  serializer_class = SportSerializer
  permission_classes = [IsAuthenticated]
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name_sport']
  ordering_fields = ['name_sport']

class EventViewSet(viewsets.ModelViewSet):
  
  queryset = Event.objects.all()
  serializer_class = EventSerializer
  permission_classes = [IsAuthenticated]
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name_event','sport__name_sport']
  ordering_fields = ['name_event','sport']
    

class GamesViewSet(viewsets.ModelViewSet):
  
  queryset = Game.objects.all()
  serializer_class = GamesSerializer
  permission_classes = [IsAuthenticated]
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name_game','season','year','city','events__name_event']
  ordering_fields = ['name_game','season','year','city','events']

class AthleteViewSet(viewsets.ModelViewSet):
  
  queryset = Athlete.objects.all()
  serializer_class = AthleteSerializer
  permission_classes = [IsAuthenticated]
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name_athlete','sex','height','weight','team__name_team']
  ordering_fields = ['name_athlete','sex','height','weight','team']

class AthleteEventsViewSet(viewsets.ModelViewSet):
  
  queryset = AthleteEvents.objects.all()
  serializer_class = AthleteEventsSerializer
  permission_classes = [IsAuthenticated]
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['athlete__name_athlete','athlete_age','game_event__game__name_game','game_event__event__name_event','medal']
  ordering_fields = ['athlete','athlete_age','game_event','medal',]


class GameEventsViewSet(viewsets.ModelViewSet):
  
  queryset = GameEvents.objects.all()
  serializer_class = GameEventsSerializer
  permission_classes = [IsAuthenticated]
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['game__name_game','event__name_event']
  ordering_fields = ['game','event']

