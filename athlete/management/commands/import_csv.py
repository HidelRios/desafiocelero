from django.core.management import BaseCommand
from django.utils import timezone
from athlete.models import *
import csv
import pandas as pd


class Command(BaseCommand):
    #help = "Load dados CSV file."

    def add_arguments(self, parser):
        parser.add_argument("file_path", type=str)

    def handle(self, *args, **options):
        start_time = timezone.now()
        file_path = options["file_path"]
        with open(file_path, "r") as csv_file:
            data = pd.read_csv(csv_file, delimiter=",").fillna(0)

            try:

                for index, row in data.iterrows():
                    team, created = Team.objects.get_or_create(
                        name_team=row['Team'],
                        noc=row['NOC']                 
                    )
                    sport, created = Sport.objects.get_or_create(
                        name_sport = row['Sport']                 
                    )
                    event, created = Event.objects.get_or_create(
                        name_event = row['Event'],
                        sport = sport               
                    )
                    game, created = Game.objects.get_or_create(
                        name_game = row['Games'],
                        season = row['Season'],
                        year = row['Year'],
                        city = row['City']
                    )

                    athlete, created = Athlete.objects.get_or_create(
                        name_athlete = row['Name'],
                        sex = row['Sex'],
                        height = row['Height'],
                        weight = row['Weight'],
                        team = team           
                    )
                    game_event, created = GameEvents.objects.get_or_create(
                        game=game,
                        event = event             
                    )
                    athlete_events, created = AthleteEvents.objects.get_or_create(
                        athlete=athlete,
                        athlete_age = row['Age'],
                        game_event = game_event,   
                        medal = row['Medal']        
                    )
                    print('row:',index)

                end_time = timezone.now()
                self.stdout.write(
                    self.style.SUCCESS(
                        f"Povoamento das tabelas realizado em: {(end_time-start_time).total_seconds()} segundos."
                    )
                )
            except KeyError:
                print("Verificar o nome das colunas da tabela. ")