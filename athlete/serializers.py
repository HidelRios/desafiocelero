from django.db.models.query import QuerySet
from rest_framework import fields, serializers
from athlete.models import *

class TeamSerializer(serializers.ModelSerializer):
  class Meta:
    model = Team
    fields = '__all__'

class SportSerializer(serializers.ModelSerializer):
  class Meta:
    model = Sport
    fields = '__all__'

class EventSerializer(serializers.ModelSerializer):
  
  sport = serializers.SlugRelatedField(
    queryset = Sport.objects.all(),
    slug_field = "name_sport"
  )

  class Meta:
    model = Event
    fields = ['id','name_event','sport']

class GamesSerializer(serializers.ModelSerializer):
  events = serializers.SlugRelatedField(
    queryset=Event.objects.all(),
    many=True,
    slug_field = "name_event"
  )

  class Meta:
    model = Game
    fields = ['id','name_game','season','year','city','events']


class AthleteSerializer(serializers.ModelSerializer):


  team = serializers.SlugRelatedField(
    queryset = Team.objects.all(),
    slug_field = "name_team"
  )
  class Meta:
    model = Athlete
    fields = ['id','name_athlete','sex','height','weight','team']



class AthleteEventsSerializer(serializers.ModelSerializer):
  athlete = serializers.SlugRelatedField(
    queryset = Athlete.objects.all(),
    slug_field = "name_athlete"
  )
  game_event = serializers.HyperlinkedRelatedField(
    queryset = Event.objects.all(),
    view_name = 'gameevents-detail',
  )

  class Meta:
    model = AthleteEvents
    fields = ['id','athlete','athlete_age','game_event','medal']

class GameEventsSerializer(serializers.ModelSerializer):
  game = serializers.SlugRelatedField(
    queryset = Game.objects.all(),
    slug_field = "name_game"
  )
  event = serializers.SlugRelatedField(
    queryset = Event.objects.all(),
    slug_field = "name_event"
  )
  class Meta:
    model = GameEvents
    fields = ['id','game','event']
