from django.test import TestCase
from athlete.models import *
import pytest

# Create your tests here.
@pytest.fixture
def new_team():
  return Team.objects.create(name_team='Brasil', noc='Br')

@pytest.fixture
def new_sport():
  return Sport.objects.create(name_sport='Futebol')

@pytest.fixture
def new_event(new_sport):
  return Event.objects.create(name_event='NomeEvento',sport=new_sport)

@pytest.mark.django_db
def test_team_create(new_team):
  assert Team.objects.count() == 1

@pytest.mark.django_db
def test_sport_create(new_sport):
  assert Sport.objects.count() == 1


@pytest.mark.django_db
def test_event_create(new_event):
  assert Event.objects.count() == 1
